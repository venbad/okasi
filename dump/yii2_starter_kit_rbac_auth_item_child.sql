INSERT INTO `yii2-starter-kit`.rbac_auth_item_child (parent, child) VALUES ('user', 'editOwnModel');
INSERT INTO `yii2-starter-kit`.rbac_auth_item_child (parent, child) VALUES ('administrator', 'loginToBackend');
INSERT INTO `yii2-starter-kit`.rbac_auth_item_child (parent, child) VALUES ('manager', 'loginToBackend');
INSERT INTO `yii2-starter-kit`.rbac_auth_item_child (parent, child) VALUES ('administrator', 'manager');
INSERT INTO `yii2-starter-kit`.rbac_auth_item_child (parent, child) VALUES ('administrator', 'user');
INSERT INTO `yii2-starter-kit`.rbac_auth_item_child (parent, child) VALUES ('manager', 'user');