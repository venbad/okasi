INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m000000_000000_base', 1566668174);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m140703_123000_user', 1566668174);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m140703_123055_log', 1566668174);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m140703_123104_page', 1566668174);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m140703_123803_article', 1566668174);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m140703_123813_rbac', 1566668174);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m140709_173306_widget_menu', 1566668174);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m140709_173333_widget_text', 1566668174);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m140712_123329_widget_carousel', 1566668175);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m140805_084745_key_storage_item', 1566668175);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m141012_101932_i18n_tables', 1566668175);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m150318_213934_file_storage_item', 1566668175);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m150414_195800_timeline_event', 1566668175);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m150725_192740_seed_data', 1566668176);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m150929_074021_article_attachment_order', 1566668176);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m160203_095604_user_token', 1566668176);
INSERT INTO `yii2-starter-kit`.system_db_migration (version, apply_time) VALUES ('m190130_155645_add_article_slug_index', 1566668176);