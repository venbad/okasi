INSERT INTO `yii2-starter-kit`.widget_menu (id, `key`, title, items, status) VALUES (1, 'frontend-index', 'Frontend index menu', '[
    {
        "label": "Get started with Yii2",
        "url": "http://www.yiiframework.com",
        "options": {
            "tag": "span"
        },
        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-success\\">{label}</a>"
    },
    {
        "label": "Yii2 Starter Kit on GitHub",
        "url": "https://github.com/yii2-starter-kit/yii2-starter-kit",
        "options": {
            "tag": "span"
        },
        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-primary\\">{label}</a>"
    },
    {
        "label": "Find a bug?",
        "url": "https://github.com/yii2-starter-kit/yii2-starter-kit/issues",
        "options": {
            "tag": "span"
        },
        "template": "<a href=\\"{url}\\" class=\\"btn btn-lg btn-danger\\">{label}</a>"
    }
]', 1);